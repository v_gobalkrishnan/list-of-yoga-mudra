package gki.out;

public class Mudra extends M{
public Mudra(String s) {
	super(s);
	
	M a1=m(Topic.specific_mudra),
      a2=m(Topic.part_1),
      a3=m(Topic.exploring_the_mudra_concept),
      a4=m(Topic.what_are_mudras),
      a5=m(Topic.origin_of_mudras),
      a6=m(Topic.breathing_visualization_and_affirmations),
      a7=m(Topic.mudras_and_music),
      a8=m(Topic.mudras_and_color),
      a9=m(Topic.using_mudras_to_heal_physical_complaints),
      a10=m(Topic.mudras_and_healing_emotional_problems),
      a11=m(Topic.mudras_and_other_hand_therapies),
      a12=m(Topic.my_personal_experiences),
      a13=m(Topic.mudras_and_meditation),
      a14=m(Topic.mudras_for_recharging_energy_reserves),
      a15=m(Topic.mudras_for_coming_to_terms_with_the_past),
      a16=m(Topic.mudras_for_improving_relationships),
      a17=m(Topic.mudras_for_solving_everyday_problems),
      a18=m(Topic.mudras_for_building_character),
      a19=m(Topic.mudras_for_planning_the_future),
      a20=m(Topic.mudras_for_connecting_with_the_divine),
      a21=m(Topic.meditations_for_each_finger),
      a22=m(Topic.meditation_1_thumb_energy),
      a23=m(Topic.meditation_2_index_finger_energy),
      a24=m(Topic.meditation_3_middle_finger_energy),
      a25=m(Topic.meditation_4_ring_finger_energy),
      a26=m(Topic.meditation_5_little_finger_energy),
      a27=m(Topic.part_2),
      a28=m(Topic.the_mudras),
      a29=m(Topic.mudras_for_the_body_mind_soul),
      a30=m(Topic.spiritual_mudras),
      a31=m(Topic.the_mudras_of_hatha_yoga),
      a32=m(Topic.part_3),
      a33=m(Topic.mudra_exercises),
      a34=m(Topic.how_to_create_your_own_mudra),
      a35=m(Topic.what_a_mudra_cannot_do),
      a36=m(Topic.a_nutrition),
      a37=m(Topic.b_there_is_an_herb_for_every_malady),
      a38=m(Topic.c_chinese_five_element_theory),
      a39=m(Topic.d_some_words_about_the_chakras),
      a40=m(Topic.a_closing_word),
      a41=m(Topic.index),
      a42=m(Topic.appendices),
      a43=m(Topic.practical_applications)
      ;
	
	
	a2.add(a3);//
	
	a3.add(a4);
	a3.add(a5);
	a3.add(a6);
	a3.add(a7);
	a3.add(a8);
	a3.add(a9);
	a3.add(a10);
	a3.add(a11);
	a3.add(a12);
	a3.add(a13);
	a3.add(a21);
	
	a13.add(a14);//
	a13.add(a15);
	a13.add(a16);
	a13.add(a17);
	a13.add(a18);
	a13.add(a19);
	a13.add(a20);
	
	
	a21.add(a22);//
	a21.add(a23);
	a21.add(a24);
	a21.add(a25);
	a21.add(a26);
	
	
	a27.add(a28);//
	a28.add(a29);
	a28.add(a30);
	a28.add(a31);
	
	
	
	a32.add(a43);//
	a43.add(a33);
	a43.add(a34);
	a43.add(a34);
	
	a42.add(a36);//
	a42.add(a37);
	a42.add(a38);
	a42.add(a39);
	a42.add(a40);
	a42.add(a41);
	
	add(a1);
	add(a2);
	add(a27);
	add(a32);
	
	a1.add(m(Topic.abhaya                   )); 
	a1.add(m(Topic.adhomukham               )); 
	a1.add(m(Topic.tse                      )); 
	a1.add(m(Topic.agnisar_kriya            )); 
	a1.add(m(Topic.agochari                 )); 
	a1.add(m(Topic.dhyana                   )); 
	a1.add(m(Topic.surabhi                  )); 
	a1.add(m(Topic.anjali_or_atmanjali      )); 
	a1.add(m(Topic.akashi                   )); 
	a1.add(m(Topic.ankush                   )); 
	a1.add(m(Topic.apan                     )); 
	a1.add(m(Topic.apanahuti                )); 
	a1.add(m(Topic.apan_vayu                )); 
	a1.add(m(Topic.adho_merudanda           )); 
	a1.add(m(Topic.ashvini                  )); 
	a1.add(m(Topic.asthma                   )); 
	a1.add(m(Topic.avahani                  )); 
	a1.add(m(Topic.back                     )); 
	a1.add(m(Topic.bhramara                 )); 
	a1.add(m(Topic.bhuchari                 )); 
	a1.add(m(Topic.bhudi                    )); 
	a1.add(m(Topic.bhujangani               )); 
	a1.add(m(Topic.bhumisparsha             )); 
	a1.add(m(Topic.bhutadamara              )); 
	a1.add(m(Topic.bija                     )); 
	a1.add(m(Topic.bilva                    )); 
	a1.add(m(Topic.bronchial                )); 
	a1.add(m(Topic.buddhasramana            )); 
	a1.add(m(Topic.chaturmukham             )); 
	a1.add(m(Topic.jnana                    )); 
	a1.add(m(Topic.detoxification           )); 
	a1.add(m(Topic.dharmachakra             )); 
	a1.add(m(Topic.matangi                  )); 
	a1.add(m(Topic.dvimukham                )); 
	a1.add(m(Topic.dynamic                  )); 
	a1.add(m(Topic.ganesha                  )); 
	a1.add(m(Topic.garuda                   )); 
	a1.add(m(Topic.grathitam                )); 
	a1.add(m(Topic.hakini                   )); 
	a1.add(m(Topic.inner_self               )); 
	a1.add(m(Topic.trailokyamohini          )); 
	a1.add(m(Topic.japa                     )); 
	a1.add(m(Topic.joint                    )); 
	a1.add(m(Topic.kurma                    )); 
	a1.add(m(Topic.kaki                     )); 
	a1.add(m(Topic.kalesvara                )); 
	a1.add(m(Topic.para                     )); 
	a1.add(m(Topic.karana                   )); 
	a1.add(m(Topic.khechari                 )); 
	a1.add(m(Topic.ksepana                  )); 
	a1.add(m(Topic.kubera                   )); 
	a1.add(m(Topic.kundalini                )); 
	a1.add(m(Topic.kunt                     )); 
	a1.add(m(Topic.lakshmi                  )); 
	a1.add(m(Topic.mahakrantam              )); 
	a1.add(m(Topic.maha_sacral              )); 
	a1.add(m(Topic.mahasirs                 )); 
	a1.add(m(Topic.makara                   )); 
	a1.add(m(Topic.matsya                   )); 
	a1.add(m(Topic.samnidhapani             )); 
	a1.add(m(Topic.mudgar                   )); 
	a1.add(m(Topic.mukula                   )); 
	a1.add(m(Topic.mushti                   )); 
	a1.add(m(Topic.naga                     )); 
	a1.add(m(Topic.navamukhi                )); 
	a1.add(m(Topic.nirvanam                 )); 
	a1.add(m(Topic.padma                    )); 
	a1.add(m(Topic.pallav                   )); 
	a1.add(m(Topic.panchmukhi               )); 
	a1.add(m(Topic.pankajam                 )); 
	a1.add(m(Topic.pralamba                 )); 
	a1.add(m(Topic.prana                    )); 
	a1.add(m(Topic.prarthana                )); 
	a1.add(m(Topic.      prithvi            ));       
	a1.add(m(Topic.      purna_gyan         ));       
	a1.add(m(Topic.      pushan             ));       
	a1.add(m(Topic.pushpaputa               )); 
	a1.add(m(Topic.rudra                    )); 
	a1.add(m(Topic.samanahuti               )); 
	a1.add(m(Topic.samputam                 )); 
	a1.add(m(Topic.samukhonmukham           )); 
	a1.add(m(Topic.sarvakarshini            )); 
	a1.add(m(Topic.saubhagyadandini         )); 
	a1.add(m(Topic.shaktam                  )); 
	a1.add(m(Topic.shakti                   )); 
	a1.add(m(Topic.shambavi                 )); 
	a1.add(m(Topic.shankh                   )); 
	a1.add(m(Topic.shanti                   )); 
	a1.add(m(Topic.shastamukham             )); 
	a1.add(m(Topic.shivalinga               )); 
	a1.add(m(Topic.singhakrantam            )); 
	a1.add(m(Topic.sthapana_karmani         )); 
	a1.add(m(Topic.suchi                    )); 
	a1.add(m(Topic.sumulkum                 )); 
	a1.add(m(Topic.sunya                    )); 
	a1.add(m(Topic.surya                    )); 
	a1.add(m(Topic.surya_pradarshani        )); 
	a1.add(m(Topic.tarjani                  )); 
	a1.add(m(Topic.tarpana                  )); 
	a1.add(m(Topic.tattva                   )); 
	a1.add(m(Topic.trimukham                )); 
	a1.add(m(Topic.udanahuti                )); 
	a1.add(m(Topic.upsanhar                 )); 
	a1.add(m(Topic.ushas                    )); 
	a1.add(m(Topic.uttarabodhi              )); 
	a1.add(m(Topic.vairagya                 )); 
	a1.add(m(Topic.vajra                    )); 
	a1.add(m(Topic.vajrahumkara             )); 
	a1.add(m(Topic.vajrapradama             )); 
	a1.add(m(Topic.venu                     )); 
	a1.add(m(Topic.varada_vara              )); 
	a1.add(m(Topic.varahaka                 )); 
	a1.add(m(Topic.varuna                   )); 
	a1.add(m(Topic.vayan                    )); 
	a1.add(m(Topic.vayu                     )); 
	a1.add(m(Topic.vistritam                )); 
	a1.add(m(Topic.vittattam                )); 
	a1.add(m(Topic.vyapakanjlikam           )); 
	a1.add(m(Topic.yamapasham               )); 
	a1.add(m(Topic.yoni                     )); 
	a1.add(m(Topic.sahajoli                 )); 
			
			
			System.out.println(a1.getLeafCount());
}
}
